.. _jsoi-schema-production:

**********************
JSOI Schema Production
**********************

Purpose
=======

This clause describes the rules for creating a schema from a MOF-based metamodel. The conformance rules are stated in
Clause :ref:`jsoi-conformance`

Namespaces and prefixes
-----------------------

JSOI follows the "explicit is better than implicit" `Zen <https://www.python.org/dev/peps/pep-0020/>`_.
For this reason implicit namesapces are **NOT** allowed in JSOI.

.. info::
    All (yes ALL) class schemas in JSON must be prefixed with a namespace uri. When available, the namespace prefix can be used. If a package does not provide **nsURI** or **nsPrefix** tags, then the schema production for that package should fail.

JSON Schema Composition
=======================

One important aspect of modelling is the ability to reuse and combine types from different metamodels.
This results in the need to support schema composition.
JSOI adpots the recommended approach for schema composition based on -`propertyNames <https://json-schema.org/understanding-json-schema/reference/object.html#property-names>`_.

Each class schema must provide an enum of it's attribute names.
Composition (inheritance) is achieved using the following structure for the inheriting schema:
1. Adding all schemas of super classes in in the *allOff* section.
2. Adding all own attributes in an object with only a *properties* section in the *allOff* section.
3. Listing all inherited attributes in the *propertyNames* section of the inheriting class.
For example:

.. code-block:: json

    {
      "someClass":
        {
          "allOf":
            [
              { "$ref": "#/definitions/jsoi:Identity" },
              { "properties":
                {
                  "name" : { "type": "string"},
                }
              },
              {
                "propertyNames":
                  {
                    "anyOf":
                      [
                        { "$ref": "#/definitions/jsoi:Identity.properties" },
                        { "$ref": "#/definitions/someClass.properties" },
                      ]
                  }
              }
           ]
        },
    "someClass.properties":
      {
        "enum": ["name"]
      },
    }

is the common pattern to use for a class schema to inherit support for JSOI identity attributes.

JSOI Schemas
============

Fixed Schema Declarations
-------------------------

There are some elements of the schema that are fixed, constituting a form of “boilerplate” necessary for every JSOI schema.
These elements are described in this sub clause.
These declarations are in the namespace “http://kinori.tech/spec/JSOI/20190306” and the JSON schema file can be found at “http://kinori.tech/spec/JSOI/20190306/JSOI.json”; alternatively these declarations can be copied into the generated schema.


Notation for EBNF
-----------------

The rule sets are stated in EBNF notation.
Each rule is numbered for reference.
Rules are given both a number and a name.
When defined, the number and name are number are cocatenated with a dot; when referenced they are linked with a colon, for example *1a. SchemaStart* and *1a:SchemaStart* respectively.
Text within quotation marks are literal values, for example “{element”.
Text enclosed in double slashes represents a placeholder to be filled in with the appropriate external value, for example //Name
of Attribute//.
Literals should be enclosed in single or double quotation marks when used as the values for JSON attributes
in JSON documents.
The suffix “?” is used to indicate repetition of an item 0 or 1 times.
The suffix “*” is used to indicate repetition of an item 0 or more times.
The suffix “+” is used to indicate repetition of an item 1 or more times.
The vertical bar “|” indicates a choice between two items.
Parentheses “()” are used for grouping items together.
EBNF ignores white space; hence these rules do not specify white space treatment.


EBNF
----

.. code-block:: ebnf

    jsoi =  draft, id, defs, properties, json;

    draft = '$schema': "//SCHEMA Draft Version//"

    id = 'id' ':' '//Metamodel URL//'

    defs = '"definitions"' ':' '{'
            { type }
            { properties }
            { typeCollections }
        '}'

    type = '"' ns ':' name '"' ':' '{'
        '{ "$ref": "#/definitions/jsoi:Identity" },'
        '{ "properties": {'
            { jsonDefinition }
        '},'
        '{ "propertyNames": {
            "anyOf": [
                { "$ref": "#/definitions/jsoi:IdentityProperties" },'
                { propertyName }
            ']
         }'
    '},'

    properties = '"' ns ':' name '.properties"' ':' '{'
        '"enum" ":"' propertyList
    '},'

    typeCollections = '"jsoi:arrays": {'
        { typeItems }
    '},'

    propertyList = '[' name {',' name}']'

    typeItems = '"' ns ':' name '"' ':' '{
        "type": "array",
        "items": [
          {' classReference '}'
          ']
        },'
