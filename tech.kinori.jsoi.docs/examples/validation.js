const Ajv = require('ajv');
const schema = require('./metamodel.json');
const ajv = new Ajv(); // options can be passed, e.g. {allErrors: true}
const validate = ajv.compile(schema);
const data = require('./model.json')
const valid = validate(data);
if (!valid) {
    console.log(validate.errors);
} else {
    console.log("all OK");
}
