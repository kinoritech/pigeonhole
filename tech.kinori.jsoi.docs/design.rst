.. _jsoi-design-principles:

******************************************
JSOI Document and Schema Design Principles
******************************************

Purpose
=======

This clause contains a description of the JSON documents produced from instances of MOF models, and JSON schemas that may be used to allow some JSON validation of these documents.
The use of schemas in JSOI is described first, followed by a brief description of some basic principles, which includes a short description of each JSON attribute and JSON element defined by JSOI.
Those descriptions are followed by more complete descriptions that provide examples illustrating the motivation for the JSOI schema design in the areas of model class specification, transmitting incomplete metadata, linking, tailoring schema production, transmitting metadata differences, and exchanging documents between tools.

It is possible to define how to automatically generate a schema from the MOF model to represent any MOF-compliant model. That definition is presented in :ref:`jsoi-schema-production`.

You may specify tag value pairs as part of the MOF model to tailor the schemas that are generated, but you are not required to do so. Using these tag value pairs requires some knowledge of JSON schemas, but the schemas that are produced might perform more validation than the default schemas. Sub clause 7.11 describes the tag values, their affect on schema production, and their impact on document serialization.

Use of JSON Schemas
===================

A JSON schema provides a means by which an JSON processor can validate the syntax and some of the semantics of a JSON document.
This Standard provides rules by which a schema can be generated for any valid JSOI- transmissible MOF-based model.
However, the use of schemas is optional; a JSON document need not reference a schema, even if one exists.
The resulting document can be processed more quickly, at the cost of some loss of confidence in the quality of the document.

Although JSON schemas are optional in general terms, it is incumbent on standards bodies that define MOF2 instances to produce corresponding JSOI Schemas for them.

It can be advantageous to perform JSON validation on the JSON document containing MOF model data.
If JSON validation is performed, any JSON processor can perform some verification, relieving import/export programs of the burden of performing these checks.
It is expected that the software program that performs verification will not be able to rely solely on JSON validation for all of the verification since JSON validation does not perform all of the verification that could be done.

Each JSN document that contains model data conforming to thisStandard contains: JSON elements that are required by this Standard, JSON elements that contain data that conform to a model, and, optionally, JSON elements that contain metadata that represent extensions of the model.
Models are explicitly identified in JSON elements required by this Standard.
Some model information can also be encoded in an JSON schema.
Performing JSON validation provides useful checking of the JSON elements that contain metadata about the information transferred, the transfer information itself, and any extensions to the model.

The JSON Schema does not provide any formal namespacing system, but we have adopted the XML Namespace specification, allowing JSOI to use multiple models at the same time.
JSON schema validation works with XML namespaces, so you can choose your own namespace prefixes in an JSON document and use a schema to validate it.
The namespace URIs, not the namespace prefixes, are used to identify which schemas to use to validate an JSON document.

JSON Validation of JSOI documents
---------------------------------

JSON validation can determine whether the JSON elements required by this Standard are present in the JSON document containing model data, whether JSON attributes that are required in these JSON elements have values for them, and whether some of the values are correct.

JSON validation can also perform some verification that the model data conforms to a model.
Although some checking can be done, it is impossible to rely solely on JSON validation to verify that the information transferred satisfies all of a model’s semantic constraints.
Complete verification cannot be done through JSON validation because it is not currently possible to specify all of the semantic constraints for a model in an JSON schema, and the rules for automatic generation of a schema preclude the use of semantic constraints that could be encoded in a schema manually, but cannot be automatically encoded.

Finally, JSON validation can be used to validate extensions to the model, because extensions must be represented as elements; if those elements are defined in a schema, the schema can be used to verify the elements.

Requirements for JSOI Schemas
-----------------------------

Each schema used by JSOI must satisfy the following requirements:

* All JSON elements and attributes defined by the JSOI specification must be imported in the schema. They cannot be put directly in the schema itself, since there is only one target namespace per schema.
* Model constructs have corresponding element declarations, and may have a JSON attribute declaration, as described below. In addition, some constructs also have a complexType declaration. The declarations may utilize groups, attribute groups, and types, as described below.
* Any JSON elements that represent extensions to the model may be declared in a schema, although it is not necessary to do so.

By default, JSOI schemas allow incomplete metadata to be transmitted, but you can enforce the lower bound of multiplicities if you wish. See 7.9 for further details.

Basic Principles
================

This sub clause discusses the basic organization of an JSON schema for JSOI. Detailed information about each of these topics is included later in this chapter.

Required JSON Declarations
--------------------------

This Standard requires that JSON element declarations, types, attributes, and attribute groups be included in schemas to enable JSON validation of metadata that conforms to this Standard.
All JSON elements defined by this International Standard are in the namespace “http://kinori.tech/spec/JSOI/*version-namespace*,” where *version-namespace* is the version of the JSOI specification being used. The XML namespace mechanism can be used to avoid name conflicts between the JSOI elements and the JSOI elements from your MOF models.

In addition to required JSON element declarations, there are some attributes that must be defined according to this Standard.
Every JSON element that corresponds to a model class must have JSON attributes that enable the JSON element to act as a proxy for a local or remote JSON element.
These attributes are used to associate a JSON element with another JSON element.
There are also other required attributes to let you put data in JSON attributes rather than JSN elements.
You may customize the declarations using MOF tag values.

Model Class Representation
--------------------------

Every model class is represented in the schema by a JSON element whose name is the class name, as well as a complexType whose name is the class name.
The declaration of the type lists the properties of the class.

By default, the content models of JSON elements corresponding to model classes do not impose an order on the properties.
By default, JSOI allows you to serialize features using either JSON elements or JSON attributes; however, JSOI allows you to specify how to serialize them if you wish. Composite and multivalued properties are always serialized using JSON elements.

JSOI Schema and Document Structure
==================================

Every JSOI Schema consists of the following attributes:

* The JSON schema uri, it can declare a specific draft. Example: ``"$schema": "http://json-schema.org/schema#"`` or ``"$schema": "http://json-schema.org/draft-07/schema#"``.
* An optional unique identifier. Example: ``$id": "http://yourdomain.com/schemas/myschema.json``
* Declarations for a specific model.

JSOI imposes no ordering requirements beyond those defined by JSON.

The top element of the JSOI information structure is either the JSOI element, or a JSON element corresponding to an
instance of a class in the MOF model. A JSON document containing only JSOI information will have JSOI as the root
element of the document.

JSOI Model
==========

This sub clause describes the model for JSOI document structure, called the JSOI model.
The JSOI model is an instance of MOF for describing the JSOI-specific information in an JSOI document, such as the version, documentation, extensions, and differences.

Using an JSOI model enables JSOI document metadata to be treated in the same fashion as other MOF metadata, allowing use of standard MOF APIs for access to and construction of JSOII-specific information in the same manner as other MOF objects.
A valid JSOI document may contain JSOI metadata but is not required to.

JSON Schema for the JSOI Model
------------------------------

When the JSOI model is generated as a JSON Schema following the JSOI schema production rules, the result is a set of JSON element and attribute declarations.
These declarations are shown in Clause :ref:`jsoi-design-principles` and given the XML namespace name of the form “http://kinori.tech/spec/JSOI/*version-namespace*,” where *version-namespace* is the XML namespace for the version of the JSOI specification being used.
Every JSOI-compliant schema must use JSOI namespace “http://kinori.texh/spec/JSOI/*version-namespace*.” when referencing types defined by JSOI.
The version of this JSOI specification is 1.0.0, and its JSOI namespace is “http://kinori.tech/spec/JSOI/20190306”,  and the JSON schema file can be found at “http://kinori.tech/spec/JSOI/20190306/JSOI.json”.

JSOI Model classes
------------------

JSOI defines a JSOI class that can be used to provide additional metadata in a JSOI document.
The JSOI class has a single attribute called documentation.
The Documentation class contains many fields to describe the document for non-computational purposes.
The DateTime primitive type has the JSON Schema string with an additional data-time format to guarantee that it is a valid RFC 3339 date.

JSOI
----

The root level JSON element for JSOI documents containing only JSOI data must be the JSOI element. Its declaration is:

.. code-block:: json

    {
      "type": "object",
      "properties": {
        "documentation": {
          "$ref": "#/definitions/Documentation"
        }
      },
      "additionalProperties": false,
    }

Documentation
-------------

The Documentation class contains information about the JSOI document or stream being transmitted, for instance the owner of the document, a contact person for the document, long and short descriptions of the document, the exporter tool which created the document, the version of the tool, the date and time the document was created, and copyright or other legal notices regarding the document.
The data type of all the attributes of Documentation is string.
The JSON Schema generated for Documentation is:

.. code-block:: json

    {
      "jsoi:Documentation": {
        "type": "object",
        "properties": {
          "id": { "type": "string" },
          "contact": { "type": "string" },
          "exporter": { "type": "string" },
          "exporterVersion": { "type": "string" },
          "longDescription": { "type": "string" },
          "shortDescription": { "type": "string" },
          "notice": { "type": "string" },
          "owner": { "type": "string" },
          "timestamp": {
            "type": "string",
            "format": "date-time"
          },
          "packageUris": {
            "type": "array",
            "items": {
              "type": "object",
              "properties": {
                "nsURI": {
                  "type": "string"
                },
                "nsPrefix": {
                  "type": "string"
                }
              },
              "additionalProperties": false
            }
          },
          "packageLocations": {
            "type": "array",
            "items" : {
              "type": "object",
              "properties": {
                "uri": { "type": "string"},
                "path": { "type": "string"}
              },
              "additionalProperties": false
            }
          }
        },
        "required": ["id", "timestamp", "packageUris"],
        "additionalProperties": false
      }
    }

JSOI Attributes
===============

This sub clause describes the fixed JSON attributes that are used in the JSOI production of JSON documents and Schemas. By defining a consistent set of JSON attributes, JSOI provides a consistent architectural structure enabling consistent object identity and linking across all assets.

To avoid name clashing without the burden of dealing with namespaces or JSON-LD, we prefix all JSOI attributes with "jsoi.".

.. _jsoi-identification:

Element Identification Attributes
---------------------------------

Three JSON attributes are defined by this Standard to identify JSON elements so that JSON elements can be associated with each other.
The purpose of these attributes is to allow JSON elements to reference other JSON elements using JSON IDREFs, XLinks, and XPointers.
Its declaration is as follows:

.. code-block:: json

    {
      "jsoi.Identity": {
        "type" : "object",
        "properties": {
          "jsoi.id": { "type": "string" },
          "jsoi.label": { "type": "string" },
          "jsoi.uuid": {
            "type": "string",
            "format": "uri" }
        }
      }
    }

:jsoi.id: JSOI semantics require the values of this attribute to be unique within an JSON document; however, the value is not required to be globally unique. This attribute may be used as the value of the idref attribute defined in the next sub clause. It may also be included as part of the value of the href attribute in XLinks. An example of the use of this attribute and the other attributes in this sub clause can be found in 7.10.3.

    If the metaclass has (or inherits) a Property with isId = ‘true,’ then the value of that property may be used as the basis of the xmi:id and/or xmi:uuid attributes.

    This is not mandatory, and the exact algorithm to be used is not specified in this Standard. However it is important, to be a valid JSOI document, that the value for xmi:id is unique across all elements within the file. The uuid attribute is not so constrained, but if the same value is used in multiple JSON elements, then they are all deemed to reference the same MOF element (e.g., they may represent different aspects).

:jsoi.label: This attribute may be used to provide a string label identifying a particular XML element. Users may put any value in this attribute.
    The value of the label attribute is ignored on import.


:jsoi.uuid: The purpose of this attribute is to provide a globally unique identifier for a JSON element. The values of this attribute should be globally unique strings prefixed by the type of identifier. If you have access to the UUID assigned in MOF, you may put the MOF UUID in the uuid JSON attribute when encoding the MOF data in JSOI.

    (FIXME)UUIDs should use URIs as the unique string. Refer to 6.4.1.1 of the MOF Facility and Object Lifecycle Specification for an example of a scheme for detailed URI production rules.

.. _jsoi-linking:

Linking Attributes
------------------

JSOI allows the use of several JSON attributes to enable JSON elements to refer to other JSN using the values of the attributes defined in the previous sub clause.
The purpose of these attributes is to allow JSON elements to act as simple XLinks or to hold a reference to an JSON element in the same document using the XML IDREF mechanism.
The attributes described in this sub clause are included in an attribute group called LinkAttribs. The attribute group declaration is:

.. code-block:: json

    {
      "jsoi:Link": {
        "type": "object",
        "oneOf" : [{
          "properties": {
            "jsoi.href": {
              "type": "string",
              "format": "uri" }
          }
        }, {
          "properties": {
            "jsoi.idref": { "type": "string" }
          }
        }]
      }
    }

The link attributes act as a union of two linking mechanisms, any one of which may be used at one time. The mechanisms are the XLink href for advanced linking across or within a document, or the idref for linking within a document.
JSOI offers another mechanism for linking, using the name of the property involved in the reference instead of href or idref. See 7.10 for more information.

.. _jsoi-href:

:jsoi.href: The href attribute declared in the above entity enables a JSON element to act in a fashion compatible with the simple XLink according to the XLink and XPointer W3C recommendations.
    The declaration and use of href is defined in the XLink and XPointer specifications.
    JSOI enables the use of simple XLinks.
    JSOI does preclude the use of extended XLinks.
    The XLink specification defines many additional XML attributes, and it is permissible to use them in addition to the attributes defined in the LinkAttribs group.

    To use simple XLinks, set jsoi.href to the URI of the desired location.
    The jsoi.href attribute can be used to reference JSON elements whose id attributes are set to particular values.

:jsoi.idref: This attribute allows a JSON element to refer to another JSON element within the same document using the XML IDREF mechanism.
    In JSOI documents, the value of this attribute should be the value of the jsoi.id attribute of the JSON element being referenced.

.. Type Attribute
.. --------------

.. The type attribute is used to specify the type of object being serialized, when the type is not known from the model.
This can occur if the type of a reference has subclasses, for instance.
The declaration of the attribute is:

.. .. code-block:: json
     {
      "Typed": {
        "type": "object",
        "properties": {
          "jsoi.type": { "type": "string" }
        }
      }
    }

.. .. todo::

.. USE SCHEMA MIX TO DO THIS Rather than including the IdentityAttribs, and LinkAttribs attribute groups, and the version and type attributes in the declarations for each MOF class, the XMI namespace includes the following declaration of the ObjectAttribs attribute group for the attribute declarations that pertain to objects:

JSOI Types
==========

The JSOI namespace contains a type called Any.
It is used in the JSOI schema production rules for class attributes, class references, and class compositions.
The declaration of this type is part of the fixed declarations for JSOI.
The Any type allows any content and any attributes to appear in elements of that type, skipping JSON validation for the element’s content and attributes.
The declaration of the type is as follows:

.. code-block:: json

    {
      "Any": {
        "type": "object",
        "additionalProperties": true
      }
    }

By using this type, the JSOI schema production rules generate smaller schemas than if this type was declared multiple times in a schema.
Also, using the Any type enables some changes to be made to the Any type declaration without affecting generated JSOI schemas.

Model Representation
====================

This sub clause describes how to represent information using JSOI:

* How classes, properties, composites, multiple elements, datatypes, and inheritance are represented in XMI compliant XML schemas.
* How instances of classes are represented in JSOI compliant JSON documents.

The production rules for these representations are given in EBNF form in the “JSON Schema Production” and “JSON Document Production” clauses.

Ids and Qualified Element Types
-------------------------------

When the official schema for a model is produced, the schema generator must use the namespace URI specified by the Package::URI property on the package representing the metamodel, which may be overridden by the org.omg.xmi.nsURI tag to identify uniquely the JSON schema in the model, via de $id attribute.
JSON processors will use $ids to identify the schemas to be used for JSON validation, as described in the JSON schema specification.
The $id (URI) of packages used in the model must be added to the *packageUris* attribute of the documentation.
This allows for fast identification of required packages.
The package's nsPrefix is used to crete unique type identifiers of the form <nsPrefix>:<TypeName>.
Optionally, the *packageLocations* can be used to point to the location of specific packages.

The following is an example of a UML model in a JSOI document using Qualified Element Types

.. code-block:: json

    {
      "documentation" : {
        "id": "the.mode.id",
        "timestamp": "2019-03-08T14:41:39+00:00",
        "packageUris": [
          {
            "nsURI":  "http://www.omg.org/spec/UML/2011070/",
            "nsPrefix": "uml"
          }
        ],
      },
      "uml:Class": [
        {
          "name": "C1",
          "ownedAttribute": [{"jsoi.idref": "a1"}]
         }
      ],
      "uml:Property": [
        {
          "jsoi.id": "a1",
          "name": "a1",
          "visibility": "private"
        }
      ]
    }

The model has a single class named **C1** that contains a single attribute named **a1** with visibility private.


Multiplicities
--------------

Model multiplicities map directly from the EMOF definition of multiplicity, which is a lower bound and an upper bound, to JSON schema array attributes called “minItems” and “maxItems”.
The minItems attribute corresponds to MultiplicityElement’s lower property, and the maxItems attribute corresponds to its upper property.
If the lower bound for a property is null, minimum multiplicity is effectively “0”.
Similarly, if the upper bound for a property is null, maximum multiplicity is not enforced in the Schema (the multiplicity is effectively unbounded).

The following is an example of a JSOI Schema attribute that defines a lower bound.

.. code-block:: json

    {
      "author": {
        "type": "array",
        "minItems": 1,
        "items": [{"$ref": "#/definitions/lib:Author"}]
    }


Class Representation
--------------------

A class is represented by an JSON element, with an JSON schema for each property, and an JSON schema enum with all property names.
The representation of a class named “c” is shown below for the simplest case where “c” does not have any Properties:

.. code-block:: json

    {
      "c" : {
        "allOf": [
        { "$ref": "#/definitions/jsoi:Identity" },
        {
          "propertyNames": {
            "anyOf": [
              { "$ref": "#/definitions/jsoi:IdentityProperties" },
            ]
          }
        }
      ]
      }
    }

If the class has properties, the JSON elements for them are placed in the properties attribute of an additional element in the
**allOff** group, and the enum of property names is added to the **propertyNames** group, as explained below.

Attribute Representation
------------------------

Properties of a class are represented by a JSON schema.
If the property types are primitives then the JSON Schema specifies the appropriate primitive type.
The mapping between JSON Schema types and the implementation type of the schema generators should follow the guidelines found in the `Type-specific keywords <https://json-schema.org/understanding-json-schema/reference/type.html>`_ section of the JSON Schema draft.
For enumerations, data-types and classes, a $ref link should be used.
The JSON object corresponding to the property is declared in the content of the schema corresponding to the class that owns the attribute.

The declaration of class "c" with the "a" property of type stting is as follows:

.. code-block:: json

    {
      "c" : {
        "allOf": [
          { "$ref": "#/definitions/jsoi:Identity" },
          { "properties": {
              "a" : { "type": "string"},
            }
          },
          {
            "propertyNames": {
              "anyOf": [
                { "$ref": "#/definitions/jsoi:IdentityProperties" },
                { "enum": ["a"] }
              ]
            }
          }
        ]
    }

For multi-valued DataType-typed Properties, the JSON Schema array is used.
When "a" is a property with enumerated values, the type used for the declaration of the JSON element and JSON attribute corresponding to the model attribute is as follows. First we define the enum:

.. code-block:: json

    {
      "definitions": {
        "streetTypes": {
          "type": "string",
          "enum": ["Street", "Avenue", "Boulevard"]
         }
      }
    }

then we can use the enum in the property:

.. code-block:: json

    {
      "properties": {
        "a" : {
          "type": "array",
          "items": {
            "$ref": "#/definitions/enumName"
          },
        }
      }
    }

The JSON Schema will never contain default values for properties.


Reference Representation
------------------------

A reference points to another model element or elements.
Each such reference is represented as a JSON schema with a $ref link.
Reference elements are declared in the content of the complexType for the class that owns the property.
This declaration enables any object to be serialized, enhancing the extensibility of models.

Composite and non-composite references are treated the same.
In both cases, the referenced model element is linked to its location in the model.
There are two possible behaviours:

* If the target element uses on of the linking attributes (see :ref:`jsoi-identification`) then the appropriate linking (see :ref:`jsoi-linking`) is used.
* If the target element does not use a linking attribute, then `JSON Path <https://goessner.net/articles/JsonPath/>`_ is used to identify the element by its location in the model

The declaration of class "c" with the "r" property (that uses :ref:`jsoi-identification`) is as follows:

.. code-block:: json

    {
      "c" : {
        "allOf": [
          { "$ref": "#/definitions/jsoi:Identity" },
          { "properties": {
              "r" : { "$ref": "#definitions/Linking" }
            }
          },
          {
            "propertyNames": {
              "anyOf": [
                { "$ref": "#/definitions/jsoi:IdentityProperties" },
                { "enum": ["r"] }
              ]
            }
          }
        ]
      }
    }


Datatype representation
------------------------

Like classes, datatypes are classifiers and can have instances that are represented by XML elements. Unlike classes,
datatypes do not have object identity, so there are no identification attributes in their representation.

.. The representation of a datatype named “dt” is shown below:

In the instance document, the value of a simple datatype appears as an attribute value.

In CMOF datatypes, other than Primitive and Enumeration Types, can have properties, which in effect allows them to be structured datatypes.
During serialization, structured datatypes are treated like classes with properties, reusing the
document production rules starting with rule 2a:XMIObjectElement (see 9.5.2) with the following adaption:

    - The name of the structured datatype is used instead of the class name.

Serializing structured datatypes is analogous to classes.

Inheritance representation
--------------------------

JSOI uses the inheritance capabilities provided by complex schema structuring supported by JSOn Schema.
A combination of **allOf** combining keyword and **propertyNames** is used to define the complete inherited properties of a class.

The declaration of class "c" with that inherits from a class "b" is as follows:

.. code-block:: json

    {
      "c" : {
        "allOf": [
          { "$ref": "#/definitions/jsoi:Identity" },
          { "$ref": "#/definitions/b" },
          { "properties": {
              "a" : { "type": "string"},
            }
          },
          {
            "propertyNames": {
              "anyOf": [
                { "$ref": "#/definitions/jsoi:IdentityProperties" },
                { "enum": ["ba", "bb"] }
                { "enum": ["a"] }
              ]
            }
          }
        ]
      }
    }

The **propertyNames** block uses a separate enum to list the properties for each inheritance.

..todo

  Can we say something about conflicts in inherited properties?

Linking
=======

The goal is to provide a mechanism for specifying references within and across documents, based on `JSON Path <https://goessner.net/articles/JsonPath/>`_.

Design principles
-----------------

* A link represents a proxy from an attribute to another element.
* Links are based on JSON Path to navigate to the element(s) within the document.
* The target document can be the current document ($) or a remote document (uri).
* Link definitions are encapsulated in the attribute group LinkAttribs defined in :ref:`_jsoi-linking`
* Links are always to elements of the same type or subclasses of that type. Restricting proxies to reference the same element type reduces complexity, enhances reliability and type safety, and promotes caching.
* When acting as a proxy only the :ref:`_jsoi-linking` link may be defined.
* Links may be chained.
* When following the link from a proxy, the definition of the proxy is replaced by the referenced element.
* It is efficient practice to use local proxies of the same element within a document to link to a single proxy that holds an external reference. For example: there could be local proxies defined for references to the predefined DataTypes such as Integer, UnlimitedNatural, String, and Boolean.

LinkAttribs supports external links through the XLink attributes, and internal links through the xmi:idref and xmi:id attributes.

Linking
-------

For JSOI, the most common linking requirements are:

* Linking to an JSON element in the same document using the element’s id.
* Linking to an JSON element in a different document using the element’s id.
* Linking to an JSON element using the element’s uuid or label, in the same or a different document.

The following sections describe how JSOI supports these requirements.

Linking within a Document
^^^^^^^^^^^^^^^^^^^^^^^^^

Every construct that can be referred to has a local JSOI ID, a string that is locally unique within a single JSON file.
Attributes representing Class-typed properties in the metamodel, or XML idref attributes can refer to other XML elements within the same XML file by specifying the target element’s XML ID.

Linking across Documents
^^^^^^^^^^^^^^^^^^^^^^^^

1. Using the JSOI href attribute to locate an JSOI id
    This is the simplest form of cross document linking.
    Here, the JSOI href attribute is used to locate a JSOI element in another JSOI document by its JSOI id.
    The value of href must be a URI reference, as defined by RFC 3986.
    The URI reference must be of the form URI#id_value, where URI locates the JSON file containing the JSON element to link to, and id_value is the value of the JSON element’s JSOI id attribute.

    As an example:
.. code-block:: json

    {
      "jsoi.id": "emp1",
      "name": "John",
      "owner": { "jsoi.href": "Co.jsoi#emp_2" }
    }

    locates JSON element ``{ jsoi.id="emp_2" ... }`` in file Co.jsoi.

2. Using an JSOI href attribute and full JSON Path to locate an JSOI uuid or label
    A JSOi href and a form of Json Path can be used to locate a JSON element in an JSON document by its JSOI uuid or label.
    This describes the form for uuid; the form for label is strictly analogous.
    * A :ref:`JSON href <jsoi-href>` attribute is used, where the uri points to the document containing the referenced element.
    * The URI reference must be an XPath for recursive descent for all JSON elements, with a subscript operator that filters by id.
      Lastly, a subscript operator is used to select only the first of the results (single reference)

        **URI#jsonpath(($..\*[?(@jsoi.uuid='value')])[1])**

    As an example:
.. code-block:: json

    {
      "jsoi.id": "emp1",
      "name": "John",
      "owner": { "jsoi.href": "Co.jsoi#jsonpath(($..\*[?(@jsoi.uuid='jsoi:emp_2')])[1])" }
    }


    locates JSON element ``{ jsoi.uuid="jsoi:emp_2" ... }`` in file Co.jsoi (as long as it is the first element with that uuid in the file).
    Since a URI can identify the same file that contains the href, this also supports locating JSON elements by JSOI uuid in the same document.

4. Using full uris and JSON Path to locate almost anything
    Full uris and JSON Path provide rich and complex capabilities for locating JSON elements, far beyond what JSOI requires.
    Consequently it is not expected that JSOI implementations supporting linking across documents provide this level of support.

Example
-------

TODO


Tailoring Schema Production
===========================

JSOI supports (in principle) schema production tailoring via *tags* as defined in Section 7.11 of the XMI Specification (Version 2.5.1)

.. TODO Add The Tag org.omg.jsoi.schemaType

General Datatype Mechanism
==========================

The ability to support general data types in JSOI has significant benefits.
The applicability of JSOI is significantly expanded since domain models are likely to have a set of domain-specific data types.
This general solution allows the user to provide a domain datatype model with a defined mapping to the JSON Schema data types.
Data types are defined in the model and the JSON serialization of the datatypes is described in terms of JSON schemas.
MOF complex data types are treated as MOF classes with each field treated as a MOF attribute with a primitive type mapped to JSON schema.
The Tag org.omg.jsoi.schemaType indicates that this class is a datatype with JSON schema mapping. The value of the tag indicates the schema type. For example, ``http://json-schema.org/schema#string`` is the string datatype.


