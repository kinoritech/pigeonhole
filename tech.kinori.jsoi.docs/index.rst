.. JSOI documentation master file, created by
   sphinx-quickstart on Wed Mar  6 22:01:42 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


JSON Metadata Interchange (JSOI) Specification
==============================================

Version 1.0.0

Scope
#####

This Standard supports the Meta Object Facility (MOF) Core defined in ISO/IEC 19508. MOF is the foundation technology for describing metamodels. It covers a wide range of domains, and is based on a constrained subset of UML. JSON Metadata Interchange (JSOI) is a JSON interchange format. It defines the following aspects involved in describing objects in JSON:

- The representation of objects in terms of JSON elements and attributes.
- The standard mechanisms to link objects within the same file or across files.
- The validation of JSOI documents using JSON Schemas.
- Object identity, which allows objects to be referenced from other objects in terms of IDs and UUIDs.

JSOI describes solutions to the above issues by specifying EBNF production rules to create JSON documents and Schemas that share objects consistently.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   conformance
   design
   schema_production
   document_production



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
