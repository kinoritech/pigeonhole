const fs = require('fs');
const Ajv = require('ajv');
const path = require('path');

// First argument is the schema

const main = fs.readFileSync(path.join(__dirname, process.argv[2]), 'utf8');
const schema_location = process.argv[3];
let schema;
if (schema_location) {
    schema = JSON.parse(fs.readFileSync(path.join(__dirname, schema_location, 'utf8')));
}
else {
    schema = require('ajv/lib/refs/json-schema-draft-07.json');
}
const ajv = new Ajv(); // options can be passed, e.g. {allErrors: true}
const validate = ajv.compile(schema);
const valid = validate(JSON.parse(main));
if (!valid) {
    console.log(validate.errors);
}
else {
    console.log('All good');
}