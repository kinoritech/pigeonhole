package tech.kinori.emf.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import org.codehaus.jackson.JsonEncoding;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonProcessingException;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EFactory;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.impl.ResourceImpl;
import org.eclipse.emf.ecore.util.ExtendedMetaData;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.emf.ecore.xml.type.XMLTypePackage;
import org.w3c.dom.Node;

import tech.kinori.emf.EClassFeature.FeatureType;
import tech.kinori.emf.Feature;
import tech.kinori.emf.FeatureTable;
import tech.kinori.emf.Features;

public class JsoiResource extends ResourceImpl implements Resource {

	Map<EClass, Set<JsonPreObject>> groups = new HashMap<>();

	public class JsonPreObject {
		private final String uriFragment;	// Maybe just save the EObject
		private final Resource resource;
		private String json;
		Features featureTable;

		public JsonPreObject(String uri, Resource resource) {
			super();
			this.uriFragment = uri;
			this.resource = resource;
			featureTable = new FeatureTable();
		}

		public void write(
			JsonGenerator jg,
			Map<EClass, Set<JsonPreObject>> groups) throws IOException {
			int contentKind;
			EObject entry = resource.getEObject(uriFragment);
			jg.writeStartObject();
			Object content = null;
			EClass eClass = entry.eClass();
			Iterator<Feature> features = featureTable.getFeatures(eClass);
			while(features.hasNext()) {
				Feature sf = features.next();
				
			
			
			
			        
				
				
				
				if (shouldSaveFeature(entry, sf)) {
					if (sf instanceof EAttribute) {
						
					} else {
						EReference er = (EReference) sf;
						if (er.isMany()) {
							@SuppressWarnings("unchecked")
							List<EObject> values = (List<EObject>) entry.eGet(er);
							if (!values.isEmpty()) {
								jg.writeFieldName(er.getName());
								jg.writeStartArray();
								for (EObject value : values) {
									writeReference(jg, value, groups);
								}
								jg.writeEndArray();
							}
						} else {
							EObject value = (EObject) entry.eGet(er);
							if (value != null && value.eResource() != null) {
								jg.writeFieldName(er.getName());
								writeReference(jg, value, groups);
							}
						}

					}
				}
			}
			jg.writeEndObject();
		}

	private void saveDataTypeSingle(JsonGenerator jg, EObject entry, EStructuralFeature sf) {
		// Persist value
		try {
			jg.writeObjectField(sf.getName(), entry.eGet(sf));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void saveDataTypeManyEmpty(JsonGenerator jg, EStructuralFeature sf) {
		// Persist value
		try {
			jg.writeObjectField(sf.getName(), Collections.emptyList());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private boolean isNil(EObject o, EStructuralFeature f) {
		return getValue(o, f) == null;
	}

	private Object getValue(EObject obj, EStructuralFeature f) {
		return obj.eGet(f, false);
	}

	protected boolean isEmpty(EObject o, EStructuralFeature f) {
		return ((List<?>) getValue(o, f)).isEmpty();
	}
	
	enum RefDoc {
		SAME_DOC, CROSS_DOC, SKIP;
	}
	
	private RefDoc sameDocSingle(EObject o, EStructuralFeature f, Resource resource) {
		InternalEObject value = (InternalEObject) getValue(o, f);
		if (value == null) {
			return RefDoc.SKIP;
		} else if (value.eIsProxy()) {
			return RefDoc.CROSS_DOC;
		} else {
			Resource res = value.eResource();
			return res == resource || res == null ? RefDoc.SAME_DOC : RefDoc.CROSS_DOC;
		}
	}

	private Object getDatatypeValue(EObject entry, EStructuralFeature sf) {
		Object value = entry.eGet(sf);
		return getDatatypeValueInternal(value, sf, true);
	}

	private String getDatatypeValueInternal(Object value, EStructuralFeature f, boolean isAttribute) {
		if (value == null) {
			return null;
		}
		EDataType d = (EDataType) f.getEType();
		EPackage ePackage = d.getEPackage();
		EFactory fac = ePackage.getEFactoryInstance();
		return fac.convertToString(d, value);
	}

	private String getDataTypeElementSingleSimple(EObject o, EStructuralFeature f) {
		Object value = o.eGet(f);
		return getDatatypeValue(value, f, false);
	}

	protected String getDatatypeValue(Object value, EStructuralFeature f, boolean isAttribute) {
		if (value == null) {
			return null;
		}
		EDataType d = (EDataType) f.getEType();
		EPackage ePackage = d.getEPackage();
		EFactory fac = ePackage.getEFactoryInstance();
		String svalue = helper.convertToString(fac, d, value);
		if (escape != null) {
			if (isAttribute) {
				svalue = escape.convert(svalue);
			} else {
				svalue = escape.convertText(svalue);
			}
		}
		return svalue;
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + getEnclosingInstance().hashCode();
		result = prime * result + ((resource == null) ? 0 : resource.hashCode());
		result = prime * result + ((uriFragment == null) ? 0 : uriFragment.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		JsonPreObject other = (JsonPreObject) obj;
		if (!getEnclosingInstance().equals(other.getEnclosingInstance()))
			return false;
		if (resource == null) {
			if (other.resource != null)
				return false;
		} else if (!resource.equals(other.resource))
			return false;
		if (uriFragment == null) {
			if (other.uriFragment != null)
				return false;
		} else if (!uriFragment.equals(other.uriFragment))
			return false;
		return true;
	}

	private void writeReference(JsonGenerator jg, EObject value, Map<EClass, Set<JsonPreObject>> groups)
			throws IOException, JsonGenerationException {

		jg.writeStartObject();
		// Create href or idref
		Set<JsonPreObject> fragments = groups.get(value.eClass());
		if (fragments == null) {
			fragments = new LinkedHashSet<>();
			groups.put(value.eClass(), fragments);
		}
		String uriFragment = value.eResource().getURIFragment(value);
		JsonPreObject pre = new JsonPreObject(uriFragment, value.eResource());
		if (!fragments.contains(pre)) {
			fragments.add(pre);
			pre.visit(groups);
		}
		if (!uriFragment.contains("#")) {
			jg.writeStringField("jsoi.idref", uriFragment);
		} else {
			jg.writeStringField("jsoi.href", String.format("$.%s:%s.items[%d]",
					value.eClass().getEPackage().getNsPrefix(), value.eClass().getName(), fragments.size() - 1));
		}
		jg.writeEndObject();
	}

	public void visit(Map<EClass, Set<JsonPreObject>> groups) throws IOException {
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		JsonFactory jsonF = new JsonFactory();
		// let's write to a file, using UTF-8 encoding (only sensible one)
		JsonGenerator jg = jsonF.createJsonGenerator(outputStream, JsonEncoding.UTF8);
		jg.useDefaultPrettyPrinter(); // enable indentation just to make debug/testing easier
		write(jg, groups);
		json = outputStream.toString();
	}

	public String json() {
		return json;
	}

	private JsoiResource getEnclosingInstance() {
		return JsoiResource.this;
	}

	}

	@Override
	protected void doSave(OutputStream outputStream, Map<?, ?> options) throws IOException {
		JsonFactory jsonF = new JsonFactory();
		// let's write to a file, using UTF-8 encoding (only sensible one)
		JsonGenerator jg = jsonF.createJsonGenerator(outputStream, JsonEncoding.UTF8);
		jg.useDefaultPrettyPrinter(); // enable indentation just to make debug/testing easier
		for (EObject entry : getContents()) {
			// write(jg, entry);
			Set<JsonPreObject> fragments = groups.get(entry.eClass());
			if (fragments == null) {
				fragments = new LinkedHashSet<>();
				groups.put(entry.eClass(), fragments);
			}
			String uriFragment = getURIFragment(entry);
			JsonPreObject pre = new JsonPreObject(uriFragment, this);
			if (!fragments.contains(pre)) {
				fragments.add(pre);
				pre.visit(groups);
			}
		}

		groups.entrySet().iterator();
		for (Entry<EClass, Set<JsonPreObject>> entry : groups.entrySet()) {
			jg.writeFieldName(
					String.format("%s:%s", entry.getKey().getEPackage().getNsPrefix(), entry.getKey().getName()));
			jg.writeStartObject();
			jg.writeNumberField("size", entry.getValue().size());
			jg.writeArrayFieldStart("items");
			for (JsonPreObject jpo : entry.getValue()) {
				jg.writeRaw(jpo.json());
			}
			jg.writeEndArray();
			jg.writeEndObject();
		}
		jg.close();
	}

}
