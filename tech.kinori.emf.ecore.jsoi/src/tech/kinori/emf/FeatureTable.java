package tech.kinori.emf;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcorePackage;

public final class FeatureTable implements Features {
	
		private final Map<String, Set<Feature>> features = new TreeMap<>();
		
		public Iterator<Feature> getFeatures(EClass cls) {
			if (!features.containsKey(cls.getName())) {
				listFeatures(cls)
					.forEach(f -> {
						if (features.containsKey(cls.getName())) {
							features.put(cls.getName(), new HashSet<>());
						}
						features.get(cls.getName()).add(featureKind(f));
					});
			}
			return features.get(cls.getName()).iterator();
		}
		
		// see XMLSaveImpl.featureKind
		protected Feature featureKind(EStructuralFeature f) {
			if (f.isTransient()) {
				return new EClassFeature(f, FeatureType.TRANSIENT);
			}
			boolean isMany = f.isMany();
			boolean isUnsettable = f.isUnsettable();
			if (f instanceof EReference) {
				EReference r = (EReference) f;
				if (r.isContainment()) {
					return containmentKind(f, isMany, isUnsettable);
				}
				EReference opposite = r.getEOpposite();
				if (opposite != null && opposite.isContainment()) {
					return new EClassFeature(f, EClassFeature.FeatureType.TRANSIENT);
				}
				return referenceKind(f, isMany, isUnsettable);
				
			} else {	// Attribute
				EDataType d = (EDataType) f.getEType();
				if (!d.isSerializable() && d != EcorePackage.Literals.EFEATURE_MAP_ENTRY) {
					return new EClassFeature(f, FeatureType.TRANSIENT);
				}
				if (isMany) {
					return new EClassFeature(f, FeatureType.DATATYPE_MANY);
				}

				if (isUnsettable) {
					return new EClassFeature(f, FeatureType.DATATYPE_SINGLE_NILLABLE);
				}
				return new EClassFeature(f, FeatureType.DATATYPE_SINGLE);
			}
		}
		
		private Set<EStructuralFeature> listFeatures(EClass eClass) {
			Set<EStructuralFeature> result = new HashSet<EStructuralFeature>();
			for (EClass eSuperType : eClass.getESuperTypes()) {
				result.addAll(listFeatures(eSuperType));
			}
			List<EAttribute> eAttributes = eClass.getEAttributes();
			result.addAll(eAttributes);
			List<EReference> eReferences = eClass.getEReferences();
			result.addAll(eReferences);
			return result;
		}
		
		private Feature containmentKind(EStructuralFeature f, boolean isMany, boolean isUnsettable) {
			FeatureType ft;
			if (isMany) {
				ft = isUnsettable ? FeatureType.OBJECT_CONTAIN_MANY_UNSETTABLE : FeatureType.OBJECT_CONTAIN_MANY;
			}
			else {
				ft = isUnsettable ? FeatureType.OBJECT_CONTAIN_SINGLE_UNSETTABLE : FeatureType.OBJECT_CONTAIN_SINGLE;
			}
			return new EClassFeature(f, ft);
		}
		
		private Feature referenceKind(EStructuralFeature f, boolean isMany, boolean isUnsettable) {
			FeatureType ft;
			if (isMany) {
				ft = isUnsettable ? FeatureType.OBJECT_HREF_MANY_UNSETTABLE : FeatureType.OBJECT_HREF_MANY;
			}
			else {
				ft = isUnsettable ? FeatureType.OBJECT_HREF_SINGLE_UNSETTABLE : FeatureType.OBJECT_HREF_SINGLE;
			}
			return new EClassFeature(f, ft);
		}
		
	}
