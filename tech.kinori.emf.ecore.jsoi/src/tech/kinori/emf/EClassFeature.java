package tech.kinori.emf;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

public class EClassFeature implements Feature {

	public enum FeatureKind {
		TRANSIENT, DATATYPE_MANY, DATATYPE_SINGLE_NILLABLE, DATATYPE_SINGLE, OBJECT_CONTAIN_SINGLE_UNSETTABLE,
		OBJECT_CONTAIN_MANY_UNSETTABLE, OBJECT_CONTAIN_SINGLE, OBJECT_CONTAIN_MANY, OBJECT_HREF_SINGLE,
		OBJECT_HREF_MANY, OBJECT_HREF_SINGLE_UNSETTABLE, OBJECT_HREF_MANY_UNSETTABLE;
	}

	private final FeatureKind kind;
	private final EStructuralFeature sf;

	public EClassFeature(EStructuralFeature feature, FeatureKind kind) {
		super();
		this.kind = kind;
		this.sf = feature;
	}

	@Override
	public EStructuralFeature structuralFeature() {
		return sf;
	}

	@Override
	public boolean saveValueFor(EObject entry) {
		if (kind != FeatureKind.TRANSIENT && shouldSaveFeature(entry)) {
			switch (kind) {
			case DATATYPE_SINGLE: {
				saveDataTypeSingle(jg, entry, sf.structuralFeature());
				continue;
			}
			case DATATYPE_SINGLE_NILLABLE: {
				if (!isNil(entry, sf.structuralFeature())) {
					saveDataTypeSingle(jg, entry, sf.structuralFeature());
					continue LOOP;
				}
				break;
			}
			case DATATYPE_MANY: {
				if (isEmpty(entry, sf.structuralFeature())) {
					saveDataTypeManyEmpty(jg, sf.structuralFeature());
					continue LOOP;
				}
				break;
			}
			case OBJECT_HREF_SINGLE_UNSETTABLE: {
				if (isNil(entry, sf.structuralFeature())) {
					break;
				}
				// it's intentional to keep going
			}
			case OBJECT_HREF_SINGLE: {
				switch (sameDocSingle(entry, sf.structuralFeature(), resource)) {
				case SAME_DOC: {
					saveIDRefSingle(o, f);
					continue LOOP;
				}
				case CROSS_DOC: {
					break;
				}
				default: {
					continue LOOP;
				}
				}
				break;
			}
			case OBJECT_HREF_MANY_UNSETTABLE: {
				if (isEmpty(o, f)) {
					saveManyEmpty(o, f);
					continue LOOP;
				}
				// It's intentional to keep going.
			}
			case OBJECT_HREF_MANY: {
				if (useEncodedAttributeStyle) {
					saveEObjectMany(o, f);
					continue LOOP;
				} else {
					switch (sameDocMany(o, f)) {
					case SAME_DOC: {
						saveIDRefMany(o, f);
						continue LOOP;
					}
					case CROSS_DOC: {
						break;
					}
					default: {
						continue LOOP;
					}
					}
				}
				break;
			}
			case OBJECT_CONTAIN_MANY_UNSETTABLE: {
				if (isEmpty(o, f)) {
					saveManyEmpty(o, f);
					continue LOOP;
				}
				break;
			}
			case OBJECT_CONTAIN_SINGLE_UNSETTABLE:
			case OBJECT_CONTAIN_SINGLE:
			case OBJECT_CONTAIN_MANY: {
				break;
			}
			default: {
				continue LOOP;
			}
			}
		}
	}

	private boolean shouldSaveFeature(EObject o) {
		return o.eIsSet(sf); // || keepDefaults && f.getDefaultValueLiteral() != null;
	}

}
