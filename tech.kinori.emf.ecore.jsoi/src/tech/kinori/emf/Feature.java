package tech.kinori.emf;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
public interface Feature {

	EStructuralFeature structuralFeature();

	boolean saveValueFor(EObject entry);
	
}
