package tech.kinori.emf;

import java.util.Iterator;

import org.eclipse.emf.ecore.EClass;

public interface Features {

	Iterator<Feature> getFeatures(EClass eClass);
	
	

}
