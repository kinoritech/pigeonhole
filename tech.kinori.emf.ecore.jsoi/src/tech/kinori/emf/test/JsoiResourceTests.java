package tech.kinori.emf.test;

import java.io.IOException;
import java.io.PrintStream;
import java.util.Collections;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.junit.jupiter.api.Test;

import tech.kinori.emf.utils.JsoiResource;

public class JsoiResourceTests {
	
	
	@Test
	public void saveMetamodel() {
		Resource r = EcorePackage.eINSTANCE.eResource();
		Resource jr = new JsoiResource();
		for (EObject eo : r.getContents()) {
			jr.getContents().add(EcoreUtil.copy(eo));
		}
		try {
			jr.save(new PrintStream(System.out, true), Collections.emptyMap());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
